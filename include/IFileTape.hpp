#ifndef I_FILE_TAPE_HPP
#define I_FILE_TAPE_HPP

#include "IOFileTape.hpp"

namespace wrk
{
  class IFileTape
  {
   public:
    IFileTape(const std::string & filename, size_t N);

    int32_t read();
    void moveForward();
    void moveBackward();
    size_t size() const;

   private:
    IOFileTape ft;
  };
}

#endif
