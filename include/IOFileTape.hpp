#ifndef IO_FILE_TAPE_HPP
#define IO_FILE_TAPE_HPP

#include <fstream>
#include <string>
#include <cstdint>
#include "Tape.hpp"

namespace wrk
{
  class IOFileTape: public Tape
  {
   public:
    IOFileTape() = delete;
    IOFileTape(const IOFileTape &) = delete;
    IOFileTape(IOFileTape &&) noexcept;
    IOFileTape & operator=(const IOFileTape &) = delete;
    IOFileTape & operator=(IOFileTape &&) noexcept;

    IOFileTape(const std::string & filename, std::ios_base::openmode openmode, size_t N);

    ~IOFileTape();

    void write(int32_t element) override;
    int32_t read() override;
    void moveForward() override;
    void moveBackward() override;
    bool isEndOfTape() const;
    size_t size() const override;

   private:
    std::size_t N;
    std::fstream file;
  };
}

#endif
