#ifndef O_FILE_TAPE_HPP
#define O_FILE_TAPE_HPP

#include "IOFileTape.hpp"

namespace wrk
{
  class OFileTape
  {
   public:
    explicit OFileTape(const std::string & filename, size_t N);

    void write(int32_t element);
    void moveForward();
    void moveBackward();
    size_t size() const;

   private:
    IOFileTape ft;
  };
}

#endif
