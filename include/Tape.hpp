#ifndef TAPE_HPP
#define TAPE_HPP

#include <cstdint>

namespace wrk
{
  class Tape
  {
   public:
    virtual ~Tape(){};

    virtual void write(int32_t element) = 0;
    virtual int32_t read() = 0;
    virtual void moveForward() = 0;
    virtual void moveBackward() = 0;
    virtual std::size_t size() const = 0;
  };
}

#endif
