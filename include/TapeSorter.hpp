#ifndef TAPE_SORTER_HPP
#define TAPE_SORTER_HPP

#include <memory>
#include "Tape.hpp"

namespace wrk
{
  class TapeSorter
  {
  public:
    TapeSorter(std::size_t memoryLimit);
    void sort(std::shared_ptr< Tape > inputTape, std::shared_ptr< Tape > outputTape) const;
  private:
    const std::size_t memoryLimit;
  };
}

#endif
