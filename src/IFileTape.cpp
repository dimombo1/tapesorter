#include "IFileTape.hpp"

wrk::IFileTape::IFileTape(const std::string & filename, size_t N):
  ft(filename, std::ios_base::in | std::ios_base::binary, N)
{}

int32_t wrk::IFileTape::read()
{
  return ft.read();
}

void wrk::IFileTape::moveForward()
{
  ft.moveForward();
}

void wrk::IFileTape::moveBackward()
{
  ft.moveBackward();
}

size_t wrk::IFileTape::size() const
{
  return ft.size();
}
