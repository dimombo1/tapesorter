#include "IOFileTape.hpp"
#include <string>

wrk::IOFileTape::IOFileTape(IOFileTape&& rhs) noexcept:
  N(rhs.N),
  file(std::move(rhs.file))
{}

wrk::IOFileTape& wrk::IOFileTape::operator=(IOFileTape&& rhs) noexcept
{
  file = std::move(rhs.file);
  N = rhs.N;
  return *this;
}

wrk::IOFileTape::IOFileTape(const std::string & filename, std::ios_base::openmode openmode, size_t N):
  N(N),
  file(filename, openmode)
{
  if (file.fail())
  {
    throw std::logic_error("Unable to create IOFileTape");
  }
}

wrk::IOFileTape::~IOFileTape()
{
  file.close();
}

void wrk::IOFileTape::write(int32_t element)
{
  file.write(reinterpret_cast< const char * >(std::addressof(element)), sizeof(element));
}

int32_t wrk::IOFileTape::read()
{
  int32_t element = 0;
  file.read(reinterpret_cast< char * >(std::addressof(element)), sizeof(element));
  if (file.fail())
  {
    throw std::logic_error("Unable to read the element");
  }
  return element;
}

void wrk::IOFileTape::moveForward()
{
  file.seekg(sizeof(int32_t), std::ios::cur);
  if (file.fail())
  {
    throw std::logic_error("Error occured while moving IOFileTape forward");
  }
}

void wrk::IOFileTape::moveBackward()
{
  file.seekg(-sizeof(int32_t), std::ios::cur);
  if (file.fail())
  {
    throw std::logic_error("Error occured while moving IOFileTape backward");
  }
}

bool wrk::IOFileTape::isEndOfTape() const
{
  return file.eof();
}

size_t wrk::IOFileTape::size() const
{
  return N;
}
