#include "OFileTape.hpp"

wrk::OFileTape::OFileTape(const std::string & filename, size_t N):
  ft(filename, std::ios_base::out | std::ios_base::binary, N)
{}

void wrk::OFileTape::write(int32_t element)
{
  ft.write(element);
}

void wrk::OFileTape::moveForward()
{
  ft.moveForward();
}

void wrk::OFileTape::moveBackward()
{
  ft.moveBackward();
}

size_t wrk::OFileTape::size() const
{
  return ft.size();
}
