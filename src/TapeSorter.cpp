#include "TapeSorter.hpp"
#include <vector>
#include <algorithm>
#include <memory>
#include <queue>
#include <cmath>
#include "Tape.hpp"
#include "IFileTape.hpp"
#include "OFileTape.hpp"
#include "IOFileTape.hpp"

#include <iostream>

wrk::TapeSorter::TapeSorter(std::size_t memoryLimit):
  memoryLimit(memoryLimit)
{}

namespace
{
  void sortChunk(std::shared_ptr< wrk::Tape > inputTape, size_t chunk_size, const std::string& filename)
  {
    std::vector< int32_t > chunk;
    chunk.reserve(chunk_size);
    for (size_t i = 0; i < chunk_size; ++i)
    {
      chunk.push_back(inputTape->read());
    }

    std::sort(chunk.begin(), chunk.end());

    wrk::OFileTape output = wrk::OFileTape(filename, chunk_size);

    for (size_t i = 0; i < chunk_size; ++i)
    {
      output.write(chunk[i]);
    }
  }

  void addPtrToTape(std::vector< std::shared_ptr< wrk::Tape > >& tapes, const std::string& filename, size_t size)
  {
    wrk::Tape * temp = new wrk::IOFileTape(filename, std::ios_base::binary | std::ios_base::in, size);
    std::shared_ptr< wrk::Tape > ptr (temp);
    tapes.push_back(ptr);
  }

  struct ValueInfo
  {
    ValueInfo(int32_t value, size_t pos, std::shared_ptr< wrk::Tape > tape):
      value(value),
      pos(pos),
      tape(tape)
    {}
    bool operator>(const ValueInfo& other) const
    {
      return value > other.value;
    }
    int32_t value;
    size_t pos;
    std::shared_ptr< wrk::Tape > tape;
  };

  void merge(std::vector< std::shared_ptr< wrk::Tape > >& tapes, std::shared_ptr< wrk::Tape > outputTape, size_t size) 
  {
    std::priority_queue< ValueInfo, std::vector< ValueInfo >, std::greater<> > minHeap;

    for (size_t i = 0; i < tapes.size(); ++i)
    {
      int32_t val = tapes[i]->read();
      std::cout << val << '\t';
      minHeap.push(ValueInfo(val, i, tapes[i])); 
    }
    std::cout << '\n';

    while (!minHeap.empty())
    {
      ValueInfo minValue = minHeap.top();
      minHeap.pop(); 
      int32_t value = minValue.value;
      size_t pos = minValue.pos;
      std::shared_ptr< wrk::Tape > tape = minValue.tape;
      outputTape->write(value);
      try
      {
        value = tape->read();
        minHeap.push(ValueInfo(value, pos, tape));
      }
      catch (const std::exception&)
      {
        tapes.erase(tapes.begin() + pos);
      }
    }
  }

  void mergeChunks(const std::vector< std::string >& filenames, size_t chunk_size, std::shared_ptr< wrk::Tape > outputTape, size_t modulo)
  {
    size_t number_of_chunks = modulo ? filenames.size() - 1 : filenames.size();
    std::vector< std::shared_ptr< wrk::Tape > > tapes;
    tapes.reserve(number_of_chunks);

    for(size_t i = 0; i < number_of_chunks; ++i) 
    {
      addPtrToTape(tapes, filenames[i], chunk_size);
    }

    addPtrToTape(tapes, filenames[filenames.size() - 1], modulo);

    merge(tapes, outputTape, chunk_size);
  }
}

void wrk::TapeSorter::sort(std::shared_ptr< wrk::Tape > inputTape, std::shared_ptr< wrk::Tape > outputTape) const
{
  size_t chunk_size = memoryLimit / sizeof(int32_t); 
  size_t number_of_chunks = inputTape->size() / chunk_size;
  size_t modulo = inputTape->size() % chunk_size;
  std::vector< std::string > filenames;
  filenames.reserve( modulo ? number_of_chunks + 1 : number_of_chunks); 

  for (size_t n = 0; n < number_of_chunks; ++n)
  {
    const std::string filename = "tmp/tmp" + std::to_string(n) + ".txt";
    filenames.push_back(filename);
    sortChunk(inputTape, chunk_size, filename);
  }
  if (modulo)
  {
    const std::string filename = "tmp/tmpfilelast.txt";
    filenames.push_back(filename);
    sortChunk(inputTape, modulo, filename);
  }
  mergeChunks(filenames, chunk_size, outputTape, modulo);
}
