#include <iostream>
#include <string>
#include <memory>
#include "Tape.hpp"
#include "IOFileTape.hpp"
#include "IFileTape.hpp"
#include "OFileTape.hpp"
#include "TapeSorter.hpp"

int main(int argc, char ** argv)
{
  const std::string WRONG_NUMBER_OF_ARGS = "WRONG_NUMBER_OF_ARGS\n";
  if (argc != 5)
  {
    std::cerr << WRONG_NUMBER_OF_ARGS;
    return 1;
  }
  try
  {
    size_t N = std::stoull(argv[1]);
    size_t M = std::stoull(argv[2]);
    std::string input_filepath = argv[3];
    std::string output_filepath = argv[4]; 
    wrk::Tape * input = new wrk::IOFileTape(input_filepath, std::ios_base::binary | std::ios_base::in, N);
    std::shared_ptr< wrk::Tape > inputTape (input);
    wrk::Tape * output = new wrk::IOFileTape(output_filepath, std::ios_base::binary | std::ios_base::out, N);
    std::shared_ptr< wrk::Tape > outpuTape (output);
    wrk::TapeSorter sorter = wrk::TapeSorter(M);
    sorter.sort(inputTape, outpuTape);
  }
  catch (const std::exception & e)
  {
    std::cerr << e.what() << '\n';
    return 2;
  }
  return 0;
}
